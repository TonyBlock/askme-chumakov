$('.js-vote').click(function(ev) {
    ev.preventDefault();
    var $this = $(this),
        action = $this.data('action'),
        id = $this.data('id'),
        type = $this.data('type');
    $.ajax('/vote/', {
        method: 'POST',
        data: {
            action: action,
            id: id,
            type: type
        }
    }).done(function(data) {
        console.log(data['error']);
        if (data['error'] == "") {
             $('.q-' + id).html(data['rating']);
        } else {
            alert('Action not permitted')
        }
        $('.js-' + id).hide();
    });

    console.log("CLICK")
});