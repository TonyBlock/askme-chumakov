from django.db.models import F
from django.http import JsonResponse
from django.utils.timezone import datetime
from django.contrib.auth.decorators import login_required
from django.contrib import auth
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from app.forms import LoginForm, SettingsForm, RegisterForm, QuestionForm
from app.models import create_paginator, Question, Answer, Tag, Profile, LikeQuestion, LikeAnswer


def index(request):
    questions = Question.object.newest_questions()
    page = create_paginator(request, questions)
    context = {"page": page}
    return render(request, 'index.html', context)

@login_required
def ask(request):
    if request.method == 'GET':
        form = QuestionForm()
    else:
        form = QuestionForm(data=request.POST)
        if form.is_valid():
            question = form.save(commit=False)
            question.user = request.user
            question.pub_date = datetime.today()
            question.save()
            for elem in form.cleaned_data['tags'].split(', '):
                tag = Tag.object.filter(name=elem)
                if elem and not tag:
                    tag = Tag(name=elem)
                    tag.save()
                else:
                    tag = tag[0]
                question.tags.add(tag.id)
            question.save()
            return redirect(reverse('question', kwargs={'pk': question.id}))
    return render(request, 'ask.html', {'form': form})


def hot(request):
    questions = Question.object.hot_questions()
    page = create_paginator(request, questions)
    tags = Tag.object.get_tags_for_questions(questions=page)
    context = {"page": page, "tags": tags}
    return render(request, 'hot.html', context)


def logout(request):
    print(request.session.pop('hello', 'nothing'))
    auth.logout(request)
    return redirect(reverse('index'))


def login(request):
    form = LoginForm(data=request.POST)
    if form.is_valid():
        user = auth.authenticate(request, **form.cleaned_data)
        if user is not None:
            auth.login(request, user)
            request.session['Hello'] = 'world'
            return redirect(reverse('index'))
    return render(request, 'login.html', {'form': form})


def signup(request):
    if request.method == 'GET':
        form = RegisterForm()
    else:
        form = RegisterForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            user = User.objects.create_user(form.cleaned_data['username'],
                                            form.cleaned_data['email'],
                                            form.cleaned_data['password1'])
            authoreq = Profile(avatar=form.cleaned_data['avatar'], user_id=user.id)
            authoreq.save()
            user = auth.authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password1'])
            auth.login(request, user)
            return redirect('index')
    return render(request, 'signup.html', {'form': form})


@login_required
def settings(request):
    if request.method == 'GET':
        form = SettingsForm(initial={'username': request.user.username, 'email':  request.user.email,
                                     'avatar': request.user.profile.avatar})
    else:
        form = SettingsForm(data=request.POST, files=request.FILES, instance=request.user)
        if form.is_valid():
            user = request.user
            user.username = form.cleaned_data['username']
            user.email = form.cleaned_data['email']
            user.profile.avatar = form.cleaned_data['avatar']
            user.profile.save()
            user.save()
            form = SettingsForm(initial={'username': request.user.username,
                                         'email': request.user.email,
                                         'avatar': request.user.profile.avatar})
    return render(request, 'settings.html', {'form': form})


def tag(request, pk):
    questions = Question.object.search_by_tag(tag=pk)
    page = create_paginator(request, questions)
    context = {"auth": True, "tag": pk, "page": page}
    return render(request, 'tag.html', context)


def question(request, pk):
    cur_question = Question.object.get_cur_question(pk)
    answers = Answer.object.get_answers(cur_question)
    page = create_paginator(request, answers)
    context = {"cur_question": cur_question, "page": page}
    return render(request, 'question.html', context)


@login_required
def answer(request):
    id = request.POST.get('pk')
    answer = Answer.object.create(user_id=request.user.id, question_id=id,
                                   pub_date=datetime.today(),
                                   text=request.POST.get('text'),
                                  is_correct=False)
    answer.save()
    return redirect(request.path)


@login_required
@require_POST
@csrf_exempt
def vote(request):
    data = request.POST
    id = data['id']
    action = data['action']
    type = data['type']
    inc = 0
    if action == "like":
        inc = 1
    elif action == "dislike":
        inc = -1
    if type == "question":
        question = Question.object.get(id=id)
        like = LikeQuestion.objects.filter(question_id=id, user_id=request.user.id)
        if question and not like:
            question.rating = F('rating') + inc
            question.save()
            LikeQuestion.objects.create(question_id=question.id, user_id=request.user.id, like=inc)
        return redirect(request.path)
    else:
        answer = Answer.object.get(id=id)
        like = LikeAnswer.objects.filter(answer_id=id, user_id=request.user.id)
        if answer and not like:
            LikeQuestion.objects.create(question_id=answer.id, user_id=request.user.id, like=inc)
        return redirect(request.path)


@login_required
@require_POST
@csrf_exempt
def check(request):
    data = request.POST
    error = ""
    answer = Answer.object.get(id=data['aid'])
    value = True
    if answer and answer.question.user.id == request.user.id:
        if answer.checked:
            answer.checked = False
            answer.save()
            value = False
        else:
            answer.checked = True
            answer.save()
    else:
        error = "None"
    return JsonResponse({'value': value})
