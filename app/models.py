from datetime import datetime
from django.core.paginator import Paginator
from django.db import models
from django.contrib.auth.models import User
from django.db.models import Count, Q, Sum
from django.db.models.functions import Coalesce

LIKE_TYPES = [
    (-1, 'Dislike'),
    (1, 'Like')
]


def create_paginator(request, paginated_content):
    paginator = Paginator(paginated_content, 5)
    page_number = request.GET.get('page')
    to_return = paginator.get_page(page_number)
    return to_return


# Create your models here.
class QuestionManager(models.Manager):
    def newest_questions(self):
        return self.annotate(answers=Count('answer'),
                             likes=Count('likequestion', filter=Q(likequestion__like_type=1)),
                             dislikes=Count('likequestion', filter=Q(likequestion__like_type=-1)))\
            .order_by('-pub_date').prefetch_related()

    def hot_questions(self):
        return self.annotate(answers=Count('answer'),
                             likes=Count('likequestion', filter=Q(likequestion__like_type=1)),
                             dislikes=Count('likequestion', filter=Q(likequestion__like_type=-1))
                             ).order_by('-rating').prefetch_related()

    def search_by_tag(self, tag):
        return self.annotate(answers=Count('answer'),
                             likes=Count('likequestion', filter=Q(likequestion__like_type=1)),
                             dislikes=Count('likequestion', filter=Q(likequestion__like_type=-1)))\
            .filter(tags__name__iexact=tag)

    def get_cur_question(self, cur_id):
        return self.annotate(likes=Count('likequestion', filter=Q(likequestion__like_type=1)),
                             dislikes=Count('likequestion', filter=Q(likequestion__like_type=-1)))\
            .get(id__exact=cur_id)


class Question(models.Model):
    title = models.CharField(max_length=50)
    text = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    pub_date = models.DateTimeField('date published', default=datetime.now)
    tags = models.ManyToManyField('Tag', related_name='questions', blank=True)
    rating = models.IntegerField(default=0, db_index=True)

    object = QuestionManager()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'


class AnswerManager(models.Manager):
    def get_answers(self, cur_question):
        return self.annotate(likes=Count('likeanswer', filter=Q(likeanswer__like_type=1)),
                             dislikes=Count('likeanswer', filter=Q(likeanswer__like_type=-1)))\
            .filter(question_id=cur_question.id)


class Answer(models.Model):
    text = models.TextField()
    is_correct = models.BooleanField()
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    pub_date = models.DateTimeField('date published')

    object = AnswerManager()

    def __str__(self):
        return "Ответ к вопросу " + str(self.question_id)

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'


class LikeQuestion(models.Model):
    like_type = models.IntegerField(choices=LIKE_TYPES, default=0)
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return LIKE_TYPES[self.like_type][1]

    class Meta:
        verbose_name = 'Оценка вопроса'
        verbose_name_plural = 'Оценки вопросов'
        #unique_together = ('question', 'user')


class LikeAnswer(models.Model):
    like_type = models.IntegerField(choices=LIKE_TYPES, default=0)
    answer = models.ForeignKey('Answer', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return LIKE_TYPES[self.like_type][1]

    class Meta:
        verbose_name = 'Оценка ответа'
        verbose_name_plural = 'Оценки ответов'
        #unique_together = ('answer', 'user')


class Profile(models.Model):
    avatar = models.ImageField(upload_to='avatar/%Y/%m/%d', default='avatar.jpg')
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'


class TagManager(models.Manager):
    def get_tags_for_questions(self, questions):
        tags = []
        for question in questions:
            tags.append({
                'question_id': question.id,
                'cur_tags': self.filter(questions__id__exact=question.id)
            })
        return tags


class Tag(models.Model):
    name = models.CharField(max_length=20)

    object = TagManager()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'
