from django import forms
from django.contrib.auth.forms import UserCreationForm
from app.models import *


class LoginForm(forms.Form):
    username = forms.CharField(max_length=20)
    password = forms.CharField(widget=forms.PasswordInput())


class QuestionForm(forms.ModelForm):
    tags = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'tag1, tag2'}))

    class Meta:
        model = Question
        fields = ['title', 'text']


class RegisterForm(UserCreationForm):
    email = forms.EmailField()
    avatar = forms.ImageField()


class SettingsForm(forms.ModelForm):
    avatar = forms.ImageField()

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'avatar']

    def save(self, *args, **kwargs):
        user = super().save(*args, **kwargs)
        user.profile.avatar = self.cleaned_data['avatar']
        user.profile.save()
        return user
