from django.core.management.base import BaseCommand
from app.models import *
from random import choice
from faker import Faker

fake = Faker()


class Command(BaseCommand):
    def answers(self, cnt):
        users_ids = list(Profile.objects.values_list('id', flat=True))
        questions_ids = list(
            Question.object.values_list('id', flat=True))
        answers = []
        for i in range(cnt):
            question_id = choice(questions_ids)
            answers.append(Answer(
                text=' '.join(fake.sentences(fake.random_int(min=5, max=10))),
                user_id=choice(users_ids),
                question_id=question_id,
                pub_date=Question.object.get(id=question_id).pub_date,
                is_correct=False
            ))
        Answer.object.bulk_create(answers)

    def handle(self, *args, **options):
        size = [20, 10001, 100001, 1000001]
        self.answers(size[2])
