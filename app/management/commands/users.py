from django.core.management.base import BaseCommand
from app.models import *
from random import choice
from faker import Faker

fake = Faker()


class Command(BaseCommand):
    pic = ['avatar.png']

    def profiles(self, cnt):
        usernames = set()
        profiles = []
        for i in range(cnt):
            username = fake.simple_profile().get('username')
            while username in usernames:
                username = fake.simple_profile().get('username')
            user = User.objects.create(username=username, password=fake.password(length=9, special_chars=True))
            profiles.append(Profile(user_id=user.id, avatar=choice(self.pic)))
            usernames.add(username)
        Profile.objects.bulk_create(profiles)

    def handle(self, *args, **options):
        size = [10001, 11000, 100001, 1000001, 900000, 1200000]
        self.profiles(size[0])
