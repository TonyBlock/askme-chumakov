from django.core.management.base import BaseCommand
from app.models import *
from django.db.models import F
from random import choice
from faker import Faker

fake = Faker()


class Command(BaseCommand):
    def fill_question_likes(self, cnt):
        questions_ids = list(
            Question.object.values_list(
                'id', flat=True
            )
        )
        user_ids = list(
            Profile.objects.values_list(
                'id', flat=True
            )
        )
        question_likes = []
        for i in range(cnt):
            question_id = choice(questions_ids)
            like_type = choice(LIKE_TYPES)[0]
            question_likes.append(LikeQuestion(
                like_type=like_type,
                user_id=choice(user_ids),
                question_id=question_id
            ))
            Question.object.filter(id=question_id).update(rating=F('rating') + like_type[0])

        LikeQuestion.objects.bulk_create(question_likes)

    def fill_answer_likes(self, cnt):
        answers_ids = list(
            Answer.object.values_list(
                'id', flat=True
            )
        )
        user_ids = list(
            Profile.objects.values_list(
                'id', flat=True
            )
        )
        answers_likes = []
        for i in range(cnt):
            answers_likes.append(LikeAnswer(
                like_type=choice(LIKE_TYPES)[0],
                user_id=choice(user_ids),
                answer_id=choice(answers_ids)
            ))
        LikeAnswer.objects.bulk_create(answers_likes)

    def handle(self, *args, **options):
        size = [10001, 100001, 1000001, 900000]
        self.fill_question_likes(size[2])
        self.fill_answer_likes(size[2])
