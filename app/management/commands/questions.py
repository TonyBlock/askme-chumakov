from django.core.management.base import BaseCommand
from app.models import *
from random import choice
from faker import Faker

fake = Faker()


class Command(BaseCommand):
    def questions(self, cnt):
        users_ids = list(Profile.objects.values_list('id', flat=True))
        tags_ids = list(Tag.object.values_list('id', flat=True))
        questions = []
        for i in range(cnt):
            questions.append(Question(
                user_id=choice(users_ids),
                text=' '.join(fake.sentences(fake.random_int(min=5, max=15))),
                title=fake.sentence()[:-1] + '?',
                pub_date=fake.date_time(),
            ))
        Question.object.bulk_create(questions)
        for q in Question.object.all():
            tag1 = Tag.object.get(id=choice(tags_ids))
            tag2 = Tag.object.get(id=choice(tags_ids))
            if tag1 != tag2:
                q.tags.add(tag1, tag2)
            else:
                q.tags.add(tag1)

    def handle(self, *args, **options):
        size = [10001, 100001, 1000001]
        self.questions(size[1])
