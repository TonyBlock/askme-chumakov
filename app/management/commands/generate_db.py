import os
from django.core.management import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        os.system("python3 manage.py users")
        os.system("python3 manage.py tags")
        os.system("python3 manage.py questions")
        os.system("python3 manage.py answers")
        os.system("python3 manage.py likes")
