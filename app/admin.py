from django.contrib import admin

# Register your models here.
from app.models import Question, Answer, LikeQuestion, LikeAnswer, Profile, Tag

admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(LikeQuestion)
admin.site.register(LikeAnswer)
admin.site.register(Profile)
admin.site.register(Tag)
