"""askme_chumakov URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from app import views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('ask/', views.ask, name="ask"),
    path('hot/', views.hot, name="hot"),
    path('login/', views.login, name="login"),
    path('signup/', views.signup, name="signup"),
    path('settings/', views.settings, name="settings"),
    path('tag/<pk>/', views.tag, name="cur_tag"),
    path('question/<int:pk>/', views.question, name="cur_question"),
    path('answer/', views.answer, name="answer"),
    path('logout/', views.logout, name="logout"),
    path('vote/', views.vote, name="vote"),
    path('index/', views.index, name="index"),
    path('', views.index, name="main_page")
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
